from django.contrib import admin
from home.models import *

class AccountAdmin(admin.ModelAdmin):
	fields=['user','name','gender']

class BookAdmin(admin.ModelAdmin):
	fields=['book_seller','title','price','condition','phone','department','book_class']

admin.site.register(Book,BookAdmin)
admin.site.register(Account)
admin.site.register(image)
admin.site.register(Blurb)
