# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect,QueryDict
from home.models import *
from home.forms import *
from django.template import Context, RequestContext, loader
from django.template.loader import get_template
from django.shortcuts import get_object_or_404, render_to_response, render, redirect
from django.forms import ModelForm
from django.db import models
from django.contrib import auth
from home.functions import mainRegValidator, valCodeGenerator
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

def main_pageNEW(request):
        if 'emailLink' in request.POST:
                form = InitRegForm(request.POST)
                if form.is_valid():
                        oldVal = RegValidator.objects.filter(user=form.cleaned_data['uname'])
                        if oldVal:
                                oldVal.delete()
                        regVal = RegValidator.objects.create(user = form.cleaned_data['uname'], valid_code = valCodeGenerator())
                        regVal.save()
                        host=request.get_host()
                        regUrl = request.get_host() + '/user/register/' + regVal.user + ('/?valid_code=%s' % regVal.valid_code)
                        email = form.cleaned_data['uname'] + '@poets.whittier.edu'
			send_email = EmailMessage('Activation Link','Here is the link to activate your account ' + regUrl,to =[email])
                        #send_email = EmailMessage('Activation Link', 'Here is your link! ' + 'www.poetexchange.com/klc93q0jc283sa3w9cjdshq898cdiuc893wasklc039cime0293clkac', to = [email])
                        send_email.send()
                        return render_to_response('success.html', {'succ1':'Now click the link that has been sent to your @poets.whittier.edu email to register!'}, context_instance=RequestContext(request))
                else:
                        return render_to_response ('error.html', {}, context_instance=RequestContext(request))
        else:
                form=InitRegForm()
                return render_to_response('home.html', {'form':form}, context_instance=RequestContext(request))


def booksNEW(request):
	books=Book.objects.all()
	myform=bookFormSEARCH()
	dept=Dept.objects.all()
	if 'q' in request.POST:
		q=request.POST['title']
		results=Book.objects.filter(title__icontains=q)
		return render_to_response('Books2.html', {'f':myform, 'books':books, 'dept':dept, 'results':results}, context_instance=RequestContext(request))
	else:
		pass
	if 'w' in request.POST:
		w=request.POST['department']
		results=Book.objects.filter(department__icontains=w)
		return render_to_response('Books2.html', {'f':myform, 'books':books, 'dept':dept, 'results':results}, context_instance=RequestContext(request))
	else:
		pass
	return render_to_response('Books.html', {'f':myform, 'books':books, 'dept':dept}, context_instance=RequestContext(request))

def sellNEW(request):
#def sellNew(request, Account_id) ADD THIS after I get it working to accept people's accounts!!!
	#if request.user.is_authenticated():
		#seller = Account.objects.filter(user_id=request.user.id)
	results=Book.objects.all()
	myform=bookForm()
	depts=Dept.objects.all()
	if 'submit' in request.POST:
		form=bookForm(request.POST)
		if form.is_valid():
			Entry=Book.objects.create(book_seller=Account.objects.get(user_id=request.user.id),title=form.cleaned_data['title'], price=form.cleaned_data['price'], condition=form.cleaned_data['condition'], phone=form.cleaned_data['phone'], department=form.cleaned_data['department'])#,book_class=form.cleaned_data['class'])
			Entry.save()
			context = {'succ1':'Good Job! You posted a book!', 'succ2':'Now you can see it if you click on the Buy Used Books tab at the top!'}
			return render_to_response('success.html', context, context_instance=RequestContext(request))
		else:
			return render_to_response('error.html', {}, context_instance=RequestContext(request))
	return render_to_response('sell.html', {'results':results, 'f':myform, 'depts':depts}, context_instance=RequestContext(request))

def buyForm(request):
	books=Book.objects.all()
	cost=Book.objects.all()
	context={'book':books, 'cost':cost}
	return render_to_response('buyForm.html', context, context_instance=RequestContext(request))

def register(request,user_slug):
	uname = user_slug
	if request.method != 'POST':
		if not mainRegValidator(request.GET,uname):
			context={}
			return render_to_response('ERROR.html',context_instance=RequestContext(request),)
	else:
		if  mainRegValidator(request.POST,uname):
			context={}
			return render_to_response('ERROR.html',context_instance=RequestContext(request),)

	form    = RegistrationForm()
	context = {'f':form}
        if request.user.is_authenticated():
                return HttpResponseRedirect('/profile/%s%s%s/' % ('5se7z529v169o', request.user.id, '2u390cf9g76yhh4'))
        if request.method == 'POST':
                form = RegistrationForm(request.POST)
                if form.is_valid():
                        #user    = User.objects.create_user(username=form.cleaned_data['username'], email = form.cleaned_data['email'] + '@poets.whittier.edu', password = form.cleaned_data['password'])
			user = User.objects.create_user(username=form.cleaned_data['username'],password = form.cleaned_data['password'],email = uname+'@poets.whittier.edu')
			user.set_password(form.cleaned_data['password'])
                        user.save()
                        drinker = Account.objects.create(user=user,username=form.cleaned_data['username'], name=form.cleaned_data['name'], gender=form.cleaned_data['gender'],email = form.cleaned_data['email'])
                        drinker.save()
                        username = form.cleaned_data['username']
                        password = form.cleaned_data['password']
			account = authenticate(username=username, password=password)
			valid = RegValidator.objects.filter(user=uname)
			valid.delete()
			if account is not None:
                                login(request, account)
                                return HttpResponseRedirect('/profile/%s%s%s' % ('5se7z529v169o', request.user.id, '2u390cf9g76yhh4'))
			else:
				return render_to_response('error.html', {}, context_instance=RequestContext(request))
                else:
                        return render_to_response('register.html', {'f':form}, context_instance=RequestContext(request))
        else:
                '''user is not submitting, so show blank reg form'''
                return render_to_response('register.html', context, context_instance=RequestContext(request))

def LoginRequest(request):
        if request.user.is_authenticated():
                return HttpResponseRedirect('/profile/%s%s%s/' % ('5se7z529v169o', request.user.id, '2u390cf9g76yhh4'))
        if request.method == 'POST':
                form = LoginForm(request.POST)
                if form.is_valid():
                        username = form.cleaned_data['username']
                        password = form.cleaned_data['password']
                        account = authenticate(username=username, password=password)
                        if account is not None:
                                login(request, account)
                                return HttpResponseRedirect('/profile/%s%s%s/' % ('5se7z529v169o', request.user.id, '2u390cf9g76yhh4'))
                        else:
                                return render_to_response('login.html', {'f':form}, context_instance=RequestContext(request))
                else:
                        return render_to_response('login.html', {'f':form}, context_instance=RequestContext(request))
        else:
                '''user not submitting, so show em the login form'''
                form = LoginForm()
                context = {'f':form}
                return render_to_response('login.html', context, context_instance=RequestContext(request))

	return render_to_response('login.html', {}, context_instance=RequestContext(request))

def logout(request):
	auth.logout(request)
	form=InitRegForm()
	return render_to_response('success.html', {'succ1':'Successfully logged out!'}, context_instance=RequestContext(request))

def profile(request, User_id):
	if request.user.is_authenticated() == False:
		return HttpResponseRedirect('/login/')
	else:
		acc = Account.objects.get(user_id=User_id)
		status = Blurb.objects.filter(username=acc)
		bks = Book.objects.filter(book_seller=acc).order_by('-book_date')
		return render_to_response('profile.html', {'Account':acc,'books':bks, 'status':status}, context_instance=RequestContext(request))

def comingsoon(request):
	return render_to_response('comingsoon.html', {}, context_instance=RequestContext(request))

def error(request):
	return render_to_response('error.html', {}, context_instance=RequestContext(request))

def success(request):
	return render_to_response('success.html', {}, context_instance=RequestContext(request))

def notes(request):
	return render_to_response('notes.html', {}, context_instance=RequestContext(request))

def dept(request, slug):
        d=get_object_or_404(Dept, abbrv = slug)
        return render_to_response('dept.html', {}, context_instance=RequestContext(request))

def blurbs(request):
	blurb=Blurb.objects.all()
	myform=BlurbForm()
	if 'submit' in request.POST:
		tempQDict = request.POST.copy()
		tempQDict.__setitem__('blurb',tempQDict.__getitem__('something'))
		form=BlurbForm(tempQDict)
		if form.is_valid():
			if form.cleaned_data['name']=='My Username':
				uname = Account.objects.get(user_id=request.user.id).username
			else:
				uname = 'Annonymous'
			if not form.cleaned_data['blurb'].isspace():
				blurb_entry = Blurb(username=Account.objects.get(user_id=request.user.id), blurber=uname, blurb=form.cleaned_data['blurb'])
				blurb_entry.save()
				return HttpResponseRedirect('/blurbs/')
        return render_to_response('blurbs.html', {'blurbs':blurb, 'f':myform, 'g':'hi yall!!'}, context_instance=RequestContext(request))

def free(request):
	if 'saleStuff' in request.POST:
		items = saleModel.objects.all()
		form = decisionForm()
		context = {'f':form,'item':items}
		return render_to_response('free.html',context,context_instance=RequestContext(request))
        elif  'k' in request.POST:
        	form = decisionForm(request.POST)
		if form.is_valid():
			if form.cleaned_data['itemType'] == 'For Sale':
				form = saleForm()
				context = {'f':form}
				return render_to_response('saleform.html', context,context_instance=RequestContext(request))
			elif form.cleaned_data['itemType'] == 'For Free':
				form = freeForm()
				context = {'f':form}
				return render_to_response('freeform.html', context,context_instance=RequestContext(request))
			else:
				items = freeModel.objects.all()
				form = decisionForm()
				context={'f':form,'item':items}
				return render_to_response('free.html',context,context_instance=RequestContext(request))
	elif 'freepost' in request.POST:
		freeform = freeForm(request.POST)
		if freeform.is_valid():
			Entry=freeModel.objects.create(seller=Account.objects.get(user_id=request.user.id),name=freeform.cleaned_data['name'],description=freeform.cleaned_data['description'],phone=freeform.cleaned_data['phone'])
			Entry.save()
			return HttpResponseRedirect('/free/')
		else:
			return render_to_response('freeform.html',{'f':freeform},context_instance=RequestContext(request))
	
	elif 'salepost' in request.POST:
		saleform = saleForm(request.POST)
		if saleform.is_valid():
			Entry=saleModel.objects.create(seller=Account.objects.get(user_id=request.user.id),name=saleform.cleaned_data['name'],description=saleform.cleaned_data['description'],phone=saleform.cleaned_data['phone'],price=saleform.cleaned_data['price'])
			Entry.save()
			return HttpResponseRedirect('/free/')
		else:
			return render_to_response('saleform.html',{'f':saleform},context_instance=RequestContext(request))
	else:
		items = freeModel.objects.all()
	        form = decisionForm()
	        context= {'f':form, 'item':items}
		return render_to_response('free.html',context,context_instance=RequestContext(request))

def free2(request):
	return HttpResponse('DELETE THIS PAGE!!')

def about(request):
	return render_to_response('about.html', {}, context_instance=RequestContext(request))

def bookpage(request, Book_id):
	bookz=Book.objects.get(id=Book_id)
        return render_to_response('Book.html', {'book':bookz}, context_instance=RequestContext(request))
