from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from home.models import *

class freeForm(ModelForm):
	name = forms.CharField(label=(u'name'), widget=forms.TextInput(attrs = {'placeholder': "Name it here, describe below"}))
	phone = forms.CharField(label=(u'phone'), widget=forms.TextInput(attrs = {'placeholder': "Enter phone # for pickup"}))
	class Meta:
		model = freeModel

class saleForm(ModelForm):
	name = forms.CharField(label=(u'name'), widget=forms.TextInput(attrs = {'placeholder': "Name it here, describe below"}))
	phone = forms.CharField(label=(u'phone'), widget=forms.TextInput(attrs = {'placeholder': "Enter phone # for pickup"}))
	price = forms.CharField(label=(u'price'), widget=forms.TextInput(attrs = {'placeholder': "How much?", 'size':'6'}))
	class Meta:
		model = saleModel

ChooseItem = (('---------','---------'),
	('For Sale','For Sale'),
	('For Free','For Free'),	
)

class decisionForm(forms.Form):
	itemType = forms.ChoiceField(choices=ChooseItem)
#	class Meta:
#		model=decide

NAMES = (
	('My Username', 'My Username'),
	('Annonymous', 'Annonymous')
)

class BlurbForm(ModelForm):
	blurb = forms.CharField(label=(u'blurb'))
#	blurb	= forms.CharField(label=(u'blurb'), widget=forms.TextInput(attrs={'placeholder':'Type a message!', 'size':'18'}))
	name	= forms.ChoiceField(choices=NAMES)
	class Meta:
		model	= Blurb

class RegistrationForm(ModelForm):
        username        = forms.CharField(label=(u'username'), widget=forms.TextInput(attrs={'placeholder':'Username'}))
	email		= forms.CharField(label=(u'Email Address'), widget=forms.TextInput(attrs={'placeholder':'Your Favorite Email'}))
	name		= forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Your Name'}))
        password        = forms.CharField(label=(u'password'), widget=forms.PasswordInput(render_value=False, attrs={'placeholder':'Password'}))
        password1       = forms.CharField(label=(u'password1'), widget=forms.PasswordInput(render_value=False, attrs={'placeholder':'Password Verification'}))

        class Meta:
                model   = Account
                exclude = ('user')

        def clean_username(self):
                username        = self.cleaned_data['username']
                try:
                        User.objects.get(username=username)
                except User.DoesNotExist:
                        return username
                raise forms.ValidationError('Username is taken')
	
	def checkempty(self):
		if self.cleaned_data['password'] == None:
                        raise forms.ValidationError('THIS ERROR MESSAGE WONT SHOW, I CANT GET IT TO WORK')
		return self.cleaned_data
	
	def checkempty(self):
		if self.cleaned_data['password1'] == None:
                        raise forms.ValidationError('THIS ERROR MESSAGE WONT SHOW, I CANT GET IT TO WORK')
		return self.cleaned_data


        def clean(self):
                if self.cleaned_data['password'] != self.cleaned_data['password1']:
                        raise forms.ValidationError('THIS ERROR MESSAGE WONT SHOW, I CANT GET IT TO WORK')
                return self.cleaned_data


class bookForm(forms.ModelForm):
	title		= forms.CharField(label=(u'Title'), widget=forms.TextInput(attrs={'placeholder':'Enter Book Title'}))
	condition		= forms.ChoiceField(label=(u'Condition'), choices=CONDITIONS)
	price		= forms.IntegerField(label=(u'Price'), widget=forms.TextInput(attrs={'placeholder':'NUMBERS ONLY!!!   (No "$")'}))
	phone		= forms.IntegerField(label=(u'Phone'), widget=forms.TextInput(attrs={'placeholder':'Or else ppl can\'t buy it!!  No - or ()'}))
#	book_class	= forms.CharField(label=(u'Class'), widget=forms.TextInput(attrs={'placeholder':'Which class is this book for?'}))
	class Meta:
		model=Book

class bookFormSELL(forms.ModelForm):
	title		= forms.CharField(label=(u'Title'), widget=forms.TextInput(attrs={'placeholder':'Enter Book Title...', 'size':'28'}))
	condition		= forms.CharField(label=(u'Condition'), widget=forms.TextInput(attrs={'placeholder':'New?  Used?  Good?...', 'size':'28'}))
	price		= forms.CharField(label=(u'Price'), widget=forms.TextInput(attrs={'placeholder':"Only Digits, no '$'...", 'size':'28'}))
	phone		= forms.CharField(label=(u'Phone'), widget=forms.TextInput(attrs={'placeholder':'Allows buyers to contact you...', 'size':'28'}))
	class Meta:
		model=Book

class bookFormSEARCH(forms.ModelForm):
	title		= forms.CharField(label=(u'Title'), widget=forms.TextInput(attrs={'placeholder':'Leave Empty to see all books'}))
	condition		= forms.CharField(label=(u'Condition'), widget=forms.TextInput(attrs={'placeholder':'New?  Used?  Good?  Worn?'}))
	price		= forms.IntegerField(label=(u'Price'), widget=forms.TextInput(attrs={'placeholder':'NUMBERS ONLY!!!   (No "$")'}))
	phone		= forms.IntegerField(label=(u'Phone'), widget=forms.TextInput(attrs={'placeholder':'Or else ppl can\'t buy it!!  No - or ()'}))
#	book_class	= forms.CharField(label=(u'Class'), widget=forms.TextInput(attrs={'placeholder':'Which class is this book for?'}))
	class Meta:
		model=Book

class InitRegForm(forms.Form) :
        uname = forms.CharField(
                                        label=(u'Please enter your my.whittier username'),
                                        max_length=8,
        widget = forms.TextInput(
                attrs = {
                        'size':3,
                        'maxlength':8,
                        'placeholder':'Username',
                }))
        def clean_uname(self):
                uname = self.cleaned_data['uname']
                try :
                        User.objects.get(email=uname+'@poets.whittier.edu')
                except User.DoesNotExist :
                        return uname
                raise forms.ValidationError('Username already registered. Please contact site admins if this is a mistake.')

class LoginForm(forms.Form):
        username        = forms.CharField(label=(u'User name'))
        password        = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False))

class StuffForm(forms.Form):
	BULLSHITTHING = models.CharField(max_length=100)

class FreeStuffForm(forms.Form):
	BULLSHITTHING = models.CharField(max_length=100)
