from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
import datetime

# Create your models here.

DECIDE = (
	('For Sale', 'For Sale'),
	('For Free', 'For Free')
)

GENDERS= (
	('M', 'M'),
	('F', 'F')
)

CONDITIONS = ( 
	('New','New'),
	('Used','Used'),
	('Good','Good'),
	('Worn','Worn')
)

DEPTCHOICES = (
	('ANTH', 'ANTH'),
	('ART', 'ART'),
	('BIOL', 'BIOL'),
	('BSAD', 'BSAD'),
	('CHDV', 'CHDV'),
	('CHEM', 'CHEM'),
	('CHIN', 'CHIN'),
	('COSC', 'COSC'),
	('ECON', 'ECON'),
	('EDUC', 'EDUC'),
	('ENGL', 'ENGL'),
	('ENST', 'ENST'),
	('ENVS', 'ENVS'),
	('FILM', 'FILM'),
	('FREN', 'FREN'),
	('GCS', 'GCS'),
	('GWS', 'GWS'),
	('HIST', 'HIST'),
	('INTD', 'INTD'),
	('JAPN', 'JAPN'),
	('KNS', 'KNS'),
	('MATH', 'MATH'),
	('MUS', 'MUS'),
	('PHIL', 'PHIL'),
	('PHYS', 'PHYS'),
	('PSYC', 'PSYC'),
	('REL', 'REL'),
	('SOC', 'SOC'),
	('SOWK', 'SOWK'),
	('SPAN', 'SPAN'),
	('THEA', 'THEA'),
	('WSP', 'WSP'),
)

class Dept(models.Model):
	abbrv=models.CharField(max_length='4')
	full=models.CharField(max_length='50')

class RegValidator(models.Model):
	'''This model holds temporary entries for user registration validation number. These entries are removed once registration is complete'''
        valid_code = models.CharField(max_length=10, unique=True,) 
	user = models.CharField(max_length=8, unique=True,)

class Account(models.Model):
	user	= models.OneToOneField(User)
	username= models.CharField(max_length=20)
	name	= models.CharField(max_length=25)
	gender	= models.CharField(max_length=1, choices=GENDERS)
	email	= models.EmailField(max_length=50)
	def __unicode__(self):
		return self.name

class image(models.Model):
	ANTH = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	ART = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	BIOL = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	BSAD = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	CHDV = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	CHEM = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	CHIN = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	COSC = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	ECON = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	EDUC = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	ENGL = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	ENST = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	ENVS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	FILM = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	FREN = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	GCS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	GWS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	HIST = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	INTD = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	JAPN = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	KNS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	MATH = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	MUS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	PHIL = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	PHYS = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	PSYC = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	REL = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	SOC = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	SOWK = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	SPAN = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	THEA = models.ImageField(upload_to='deptpics/', null=True, blank=True)
	WSP = models.ImageField(upload_to='deptpics/', null=True, blank=True)



class Book(models.Model):
	book_seller= models.ForeignKey('home.Account',blank=True, null = True)
	title=models.CharField(max_length=200)
	price=models.IntegerField(max_length=4)
	condition=models.CharField(max_length=5,choices=CONDITIONS)
	phone=models.IntegerField(max_length=20)
	department=models.CharField(max_length=4, choices=DEPTCHOICES)
	book_class=models.CharField(max_length=10,blank= True, null = False)
	book_date = models.DateTimeField(editable=False)
	def __unicode__(self):
		return self.title
	def save(self, *args, **kwargs):
		self.book_date=datetime.datetime.today()
		super(Book, self).save(*args, **kwargs)

class Blurb(models.Model):
	username = models.ForeignKey('home.Account', blank=True, null=True)
	blurber = models.CharField(max_length=20, blank=True,null=True)
	blurb	= models.CharField(max_length=80, unique=True)
	def __unicode__(self):
		return self.blurb

class freeModel(models.Model):
	seller = models.ForeignKey('home.Account',blank=True, null=True)
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=500,blank=True,null=True)
	phone = models.IntegerField(max_length=15)
	date = models.DateTimeField(editable=False)
	def __unicode__(self):
		return self.name
	def save(self, *args, **kwargs):
		self.date=datetime.datetime.today()
		super(freeModel, self).save(*args, **kwargs)

class saleModel(models.Model):
	seller = models.ForeignKey('home.Account',blank=True,null=True)
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=500,blank=True,null=True)
	phone = models.IntegerField(max_length=15)
	price = models.IntegerField(max_length=9)
	date = models.DateTimeField(editable=False)
	def __unicode__(self):
		return self.name
	def save(self, *args, **kwargs):
		self.date = datetime.datetime.today()
		super(saleModel, self).save(*args, **kwargs)
