from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'home.views.main_pageNEW', name='home'),
    url(r'^books', 'home.views.booksNEW', name='books'),
    url(r'^book/(?P<Book_id>\d+)/$', 'home.views.bookpage', name='bookpage'),
    url(r'^sellb', 'home.views.sellNEW', name='sellb'),
    url(r'^comingsoon', 'home.views.comingsoon', name='comingsoon'),
    url('^error', 'home.views.error'),
    url('^notes', 'home.views.notes'),
    url('^free', 'home.views.free', name='free'),
    url('^free2', 'home.views.free2', name='free2'),
    url('^blurbs', 'home.views.blurbs', name='blurbs'),
    url('^success', 'home.views.success'),
	################## LOGIN ####################
    url('^user/register/(?P<user_slug>[a-z0-9]{2,8})/$','home.views.register',name='register'),
    url('^login', 'home.views.LoginRequest', name='login'),
    url('^logout', 'home.views.logout', name='logout'),
    url(r'^profile/5se7z529v169o(?P<User_id>\d+)2u390cf9g76yhh4', 'home.views.profile', name='profile'),
 #   url(r'^pages', 'pages.views.pages'),
    # Uncomment the admin/doc line below to enable admin documentation:
  #  url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
